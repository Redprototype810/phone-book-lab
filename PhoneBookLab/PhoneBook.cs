﻿using System;
using System.Collections.Generic;

namespace PhoneBookLab
{
    class PhoneBook
    {

        private Dictionary<string, Record> records = new Dictionary<string, Record>();

        static void Main(string[] args)
        {
            Console.WriteLine("================================================");
            Console.WriteLine("===== Телефонная книга Кости Кузнецова (с) =====");
            Console.WriteLine("================================================");
            new PhoneBook().ShowMenu();
        }

        private void ShowMenu()
        {
            Console.WriteLine("\nВыберите команду:");
            Console.WriteLine("1 - Создать новую запись");
            Console.WriteLine("2 - Редактировать запись");
            Console.WriteLine("3 - Удалить запись");
            Console.WriteLine("4 - Просмотреть запись");
            Console.WriteLine("5 - Просмотреть все записи");
            HandleCommand(Console.ReadLine());
        }

        private void HandleCommand(string commandStr)
        {
            int command;
            int.TryParse(commandStr, out command);
            switch (command)
            {
                case 1:
                    CreateRecord();
                    break;
                case 2:
                    EditRecord();
                    break;
                case 3:
                    DeleteRecord();
                    break;
                case 4:
                    ShowRecord();
                    break;
                case 5:
                    ShowAllRecords();
                    break;
                default:
                    Console.WriteLine("Введена недопустимая команда. Попробуйте снова.");
                    break;
            }
            ShowMenu();
        }

        private void CreateRecord()
        {
            Console.WriteLine(
                "\n------------------------------------------------" +
                "\nСоздание новой записи." +
                "\nПоля отмеченные (*) обязательны для заполнения." +
                "\n------------------------------------------------"
            );
            Record record = new RecordCreator().CreateRecord();
            records.Add(record.LastName, record);
            Console.WriteLine(
                "\n------------------------------------------------" +
                "\nЗапись \"" + record.LastName + "\" создана!" +
                "\n------------------------------------------------"
            );
        }

        private void EditRecord()
        {
            if (CheckIfBookEmpty())
            {
                return;
            }
            Console.Write("\nВведите фамилию: ");
            string lastName = Console.ReadLine().Trim();
            Record record;
            if (records.TryGetValue(lastName, out record))
            {
                Console.WriteLine(
                   "\n------------------------------------------------" +
                    "\nРедактирование записи \"" + lastName + "\":"
                );
                Console.WriteLine(record);
                EditRecord(lastName);
            }
            else
            {
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nЗапись \"" + lastName + "\" не найдена!" +
                    "\n------------------------------------------------"
                );
            }
        }

        private void EditRecord(string lastName)
        {
            Console.WriteLine(
                "\nПоля отмеченные (*) обязательны для заполнения." +
                "\n------------------------------------------------"
            );
            Record record = new RecordCreator().CreateRecord();
            if (record.LastName == lastName)
            {
                records[lastName] = record;
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nЗапись \"" + lastName + "\" отредактирована!" +
                    "\n------------------------------------------------"
                );
            }
            else
            {
                records.Remove(lastName);
                records.Add(record.LastName, record);
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nЗапись \"" + lastName + "\" удалена," +
                    "\nЗапись \"" + record.LastName + "\" добавлена!" +
                    "\n------------------------------------------------"
                );
            }

        }

        private void DeleteRecord()
        {
            if (CheckIfBookEmpty())
            {
                return;
            }
            Console.Write("\nВведите фамилию: ");
            DeleteRecord(Console.ReadLine().Trim());
        }

        private void DeleteRecord(string lastName)
        {
            Console.WriteLine("\n-----------------------------------------------");
            if (records.Remove(lastName))
            {

                Console.WriteLine("Запись \"" + lastName + "\" удалена!");
            }
            else
            {
                Console.WriteLine("Запись \"" + lastName + "\" не найдена!");
            }
            Console.WriteLine("-----------------------------------------------");
        }

        private void ShowRecord()
        {
            if (CheckIfBookEmpty())
            {
                return;
            }
            Console.Write("\nВведите фамилию: ");
            ShowRecord(Console.ReadLine().Trim());
        }

        private void ShowRecord(string lastName)
        {
            Record record;
            if (records.TryGetValue(lastName, out record))
            {
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nЗапись \"" + lastName + "\" найдена:" +
                    "\n------------------------------------------------"
                );
                Console.WriteLine(record);
                Console.Write("\nНажмите Enter чтобы вернуться в меню. ");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nЗапись \"" + lastName + "\" не найдена!" +
                    "\n------------------------------------------------"
                );
            }
        }

        private void ShowAllRecords()
        {
            if (CheckIfBookEmpty())
            {
                return;
            }
            else
            {
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nНайдены записи:" +
                    "\n------------------------------------------------"
                );
                foreach (Record record in records.Values)
                {
                    Console.WriteLine(record);
                    Console.WriteLine("------------------------------------------------");
                }
                Console.Write("Нажмите Enter чтобы вернуться в меню. ");
                Console.ReadLine();
            }
        }

        private bool CheckIfBookEmpty()
        {
            if (records.Count == 0)
            {
                Console.WriteLine(
                    "\n------------------------------------------------" +
                    "\nТелефонная книга пуста!" +
                    "\n------------------------------------------------"
                );
                return true;
            }
            return false;
        }

    }

    class Record
    {
        public string LastName { get; private set; }
        public string FirstName { get; private set; }
        public string Patronymic { get; private set; }
        public long Phone { get; private set; }
        public string Country { get; private set; }
        public string BirthDate { get; private set; }
        public string Company { get; private set; }
        public string Duty { get; private set; }
        public string Note { get; private set; }

        public Record(string lastName, string firstName, string patronymic, long phone,
            string country, string birthDate, string company, string duty, string note)
        {
            LastName = lastName;
            FirstName = firstName;
            Patronymic = patronymic;
            Phone = phone;
            Country = country;
            BirthDate = birthDate;
            Company = company;
            Duty = duty;
            Note = note;
        }

        public override string ToString()
        {
            string output = "Имя: " + FirstName + "\nФамилия: " + LastName;
            if (Patronymic != "")
            {
                output += "\nОтчество: " + Patronymic;
            }
            output += "\nНомер телефона: " + Phone;
            if (Country != "")
            {
                output += "\nСтрана: " + Country;
            }
            if (BirthDate != "")
            {
                output += "\nДата рождения: " + BirthDate;
            }
            if (Company != "")
            {
                output += "\nОрганизация: " + Company;
            }
            if (Duty != "")
            {
                output += "\nДолжность: " + Duty;
            }
            if (Note != "")
            {
                output += "\nПримечание: " + Note;
            }
            return output;
        }
    }

    class RecordCreator
    {
        private InputReader reader = new InputReader();
        public Record CreateRecord()
        {
            string lastName = reader.ReadLastName();
            string firstName = reader.ReadFirstName();
            string patronymic = reader.ReadPatronymic();
            long phone = reader.ReadPhone();
            string country = reader.ReadCountry();
            string birthDate = reader.ReadBirthDate();
            string company = reader.ReadCompany();
            string duty = reader.ReadDuty();
            string note = reader.ReadNote();
            return new Record(lastName, firstName, patronymic, phone, country, birthDate, company, duty, note);
        }
    }

    class InputReader
    {
        public string ReadLastName()
        {
            string lastName;
            while (true)
            {
                Console.Write("Фамилия (*): ");
                lastName = Console.ReadLine().Trim();
                if (lastName == "")
                {
                    Console.WriteLine("Фамилия не может быть пустой!");
                }
                else
                {
                    break;
                }
            }
            return lastName;
        }

        public string ReadFirstName()
        {
            string firstName;
            while (true)
            {
                Console.Write("Имя (*): ");
                firstName = Console.ReadLine().Trim();
                if (firstName == "")
                {
                    Console.WriteLine("Имя не может быть пустым!");
                }
                else
                {
                    break;
                }
            }
            return firstName;
        }

        public string ReadPatronymic()
        {
            Console.Write("Отчество: ");
            return Console.ReadLine().Trim();
        }

        public long ReadPhone()
        {
            long phone;
            while (true)
            {
                Console.Write("Номер телефона (*): ");
                string phoneInput = Console.ReadLine().Trim();
                if (phoneInput == "")
                {
                    Console.WriteLine("Номер не может быть пустым!");
                }
                else if (long.TryParse(phoneInput, out phone) == false)
                {
                    Console.WriteLine("Номер должен содержать только цифры!");
                }
                else
                {
                    break;
                }
            }
            return phone;
        }

        public string ReadCountry()
        {
            Console.Write("Страна: ");
            return Console.ReadLine().Trim();
        }

        public string ReadBirthDate()
        {
            Console.Write("Дата рождения: ");
            return Console.ReadLine().Trim();
        }

        public string ReadCompany()
        {
            Console.Write("Организация: ");
            return Console.ReadLine().Trim();
        }

        public string ReadDuty()
        {
            Console.Write("Должность: ");
            return Console.ReadLine().Trim();
        }

        public string ReadNote()
        {
            Console.Write("Примечание: ");
            return Console.ReadLine().Trim();
        }
    }

}
